require 'test_helper'
class UserTest < ActiveSupport::TestCase
  def setup
    @marcin = User.new(name: "Marcin", email: "marcin@stefaniak.com", password: "haslo123", password_confirmation: "haslo123")
  end
  test "should be valid" do
    assert @marcin.valid?
  end
  test "name should be invalid" do
    no = @marcin
    no.name = ""
    assert_not no.valid?
    no.name = "a"*36
    assert_not no.valid?
  end
  test "email should be invalid" do
    no = @marcin
    email = ["test", "", "test@test", "test@test@test"]
    email.each do |e|
      no.email = e
      no.valid?
    end
  end
  test "password should be invalid" do
    no = @marcin
    no.password_confirmation = ""
    assert_not no.valid?
    no.password = ""
    assert_not no.valid?
    no.password = "a"*7
    no.password_confirmation = "a"*7
    assert_not no.valid?
    no.password = "a"*266
    no.password_confirmation = "a"*266
    assert_not no.valid?
  end
end

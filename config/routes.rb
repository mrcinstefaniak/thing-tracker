Rails.application.routes.draw do
  resources :users
  resources :trackers
  get '/login', to: 'sessions#new'
  post '/login', to: 'sessions#create'
  delete '/logout', to: 'sessions#destroy'
  get 'page/about'
  get 'page/help'
  root 'page#home'
end

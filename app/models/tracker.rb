class Tracker < ApplicationRecord
  belongs_to :user
  has_many :points

  validates :name,    presence: true,
                      length: {maximum: 35}
  validates :simple,  presence: true
end

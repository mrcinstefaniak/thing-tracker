class User < ApplicationRecord
  has_many :trackers
  has_one_attached :avatar
  before_save(:email.downcase)

  validates :name,      presence: true,
                        length: {maximum: 35}
  validates :email,     presence: true,
                        length: {maximum: 255},
                        format: {with: /\A[\w+\-.]+@[a-z\d\-.]+\.[a-z]+\z/i},
                        uniqueness: {case_sensitive: false}
  validates :password,  length: {in: 8..255}
  has_secure_password
end

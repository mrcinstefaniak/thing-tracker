class UsersController < ApplicationController
  def index
  end

  def show
    @user = User.find(params[:id])
    if logged? && current_user == @user
      @tracker_new = current_user.trackers.new
    end
  end

  def new
    @user = User.new()
  end

  def create
    @user = User.new(user_params)
    if @user.save
      flash[:success] = "You have succesly created account"
      log_in @user
      redirect_to root_url
    else
      flash.now[:danger] = "You have an error in your sign form"
      render 'new'
    end
  end

  def destroy
  end

  def edit
    @user = current_user
  end

  def update
    @user = current_user
    if name = params[:user][:name]
      @user.name = name
    elsif avatar = params[:user][:avatar]
      @user.avatar = avatar
    end
    @user.password = params[:user][:password]
    @user.save
    redirect_to user_path
  end

  private

    def user_params
      params.require(:user).permit(:name, :email, :password, :password_confirmation)
    end
end

class TrackersController < ApplicationController
  def show
    @tracker = Tracker.find(params[:id])
  end

  def create
    if logged?
      @tracker = current_user.trackers.new
      @tracker.name = params[:tracker][:name]
      if params[:tracker][:simple] == "1"
        @tracker.simple = 1
      else
        @tracker.simple = 0
      end
      @tracker.chart_type = params[:tracker][:chart_type]
      @tracker.color = params[:tracker][:color]
      if @tracker.save
        redirect_to @tracker
      else
        redirect_to root_url
      end
    end
  end

  def destroy
    if logged? && current_user.id = Tracker.find(params[:id]).user_id
      Tracker.find(params[:id]).destroy
    end
    redirect_back fallback_location: root_url
  end

  def update
    @tracker = Tracker.find(params[:id])
    if logged? && current_user.id == @tracker.user_id
      if @tracker.simple == 0
        if params[:t][:date] && params[:t][:points]
          dat = params[:t][:date].to_date
          val = params[:t][:points]
          if p = @tracker.points.find_by(created_at: dat.beginning_of_day..dat.end_of_day)
            p.update(value: val)
          else
            if dat < Date.current.end_of_day
              @tracker.points.create(value: val, created_at: dat)
            else
              flash.now[:danger] = "You can't commit future dates"
            end
          end
        end
      else
        if params[:date]
          dat = params[:date].to_date
          if t = @tracker.points.find_by(created_at: dat)
            t.destroy
          else
            @tracker.points.create(created_at: dat, value: 1)
          end
        end
      end
    end
    render 'show'
  end
end

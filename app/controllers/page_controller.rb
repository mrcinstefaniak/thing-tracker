class PageController < ApplicationController
  def home
    if logged?
      @tracker_new = current_user.trackers.new
    end
  end

  def about
  end

  def help
  end
end

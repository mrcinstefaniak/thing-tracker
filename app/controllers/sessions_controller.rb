class SessionsController < ApplicationController
  def new
  end

  def create
    user = User.find_by(email: params[:session][:email])
    if user && user.authenticate(params[:session][:password])
      log_in user
      flash[:success] = "Welcome back #{current_user.name}"
      redirect_to root_path
    else
      flash.now[:danger] = "Wrong email and/or password"
      render 'new'
    end
  end

  def destroy
    log_out
    flash[:success] = "You have logged out"
    redirect_to root_path
  end
end

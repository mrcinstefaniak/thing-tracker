class TrackerRepair < ActiveRecord::Migration[5.2]
  def change
    rename_column :trackers, :type, :simple
  end
end

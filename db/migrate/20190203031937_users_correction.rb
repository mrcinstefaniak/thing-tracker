class UsersCorrection < ActiveRecord::Migration[5.2]
  def change
    remove_column :users, :password_confirmation
    rename_column :users, :password, :password_digest
  end
end

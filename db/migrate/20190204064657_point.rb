class Point < ActiveRecord::Migration[5.2]
  def change
    rename_column :points, :tracer_id, :tracker_id
  end
end

class AddColorColumnToTracker < ActiveRecord::Migration[5.2]
  def change
    add_column :trackers, :color, :string
  end
end

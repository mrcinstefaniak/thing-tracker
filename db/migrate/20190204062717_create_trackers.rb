class CreateTrackers < ActiveRecord::Migration[5.2]
  def change
    create_table :trackers do |t|
      t.string :name
      t.integer :user_id
      t.integer :type
      t.integer :chart_type

      t.timestamps
    end
  end
end

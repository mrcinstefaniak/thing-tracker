class ChangingTrackerLogic < ActiveRecord::Migration[5.2]
  def change
    add_column :points, :value, :decimal, precision: 10, scale: 3
  end
end
